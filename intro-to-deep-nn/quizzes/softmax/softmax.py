import numpy as np

# Write a function that takes as input a list of numbers, and returns
# the list of values given by the softmax function.
def softmax(L):
    sum_of_exp = np.sum(np.exp(L))
    SoftmaxList = L/np.full((len(L)),sum_of_exp)
    return SoftmaxList