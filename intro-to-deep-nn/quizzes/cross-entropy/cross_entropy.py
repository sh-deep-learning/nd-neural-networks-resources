import numpy as np

# Write a function that takes as input two lists Y, P,
# and returns the float corresponding to their cross-entropy.
def cross_entropy(Y, P):
    y = np.asarray(Y)
    p = np.asarray(P)
    s = np.multiply(y, np.log(p)) + np.multiply(1-y, np.log(1-p))
    sum = np.sum(s)
    ce = np.multiply(sum, -1)
    return ce